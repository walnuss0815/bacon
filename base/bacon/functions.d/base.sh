#!/bin/sh

start() {
    ## Create repo if it doesn't exist
    restic snapshots --repo $RESTIC_REPOSITORY || restic init --repo $RESTIC_REPOSITORY

    ## Start cron
    echo "$CRON /bacon/bacon.sh backup" > /bacon/crontab
    echo "$FORGET_CRON /bacon/bacon.sh prune" >> /bacon/crontab
    supercronic /bacon/crontab
}

base_backup() {
    ## Create backup
    backup

    ## File backup
    restic -r $RESTIC_REPOSITORY --verbose backup $DATA_DIR
}

base_restore() {
    ## Check if snapshot supplied
    if [ -z "$1" ]
    then
        echo "No snapshot supplied"
        exit 1
    fi
    SNAPSHOT=$1

    ## File restore
    restic --repo $RESTIC_REPOSITORY restore $SNAPSHOT --target $DATA_DIR/..

    ## Restore
    restore
}

prune() {
    if [ -z "$FORGET_PARAMS" ]
    then
        echo "Variable FORGET_PARAMS not set - Skipping forget"
    else
        echo "Execute forget with params $FORGET_PARAMS"
        restic forget $FORGET_PARAMS
    fi
}

snapshots() {
    ## Print snapshots
    restic snapshots --repo $RESTIC_REPOSITORY 
}

base_help() {
    SCRIPT_NAME=$(basename -- "$0")

    echo "Usage:"
    echo "$SCRIPT_NAME COMMAND"
    echo
    echo "  start - Start cronjob"
    echo "  backup - Create backup"
    echo "  restore SNAPSHOT - Restore backup"
    echo "  snapshots - List snapshots"
    echo "  prune - Prune backups"
    help
}