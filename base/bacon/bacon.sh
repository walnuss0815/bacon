#!/bin/sh

for f in ./functions.d/*; do source $f; done

COMMAND=$1

echo "BaCon: $COMMAND"

case "$COMMAND" in
start)
	start
	break
	;;
backup)
	base_backup
	break
	;;
restore)
	base_restore $2
	break
	;;
snapshots)
	snapshots
	break
	;;
prune)
	prune
	break
	;;
*)
	select_command $@
	;;
esac
