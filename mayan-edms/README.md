# Mayan EDMS

## Usage
Restore files to data directory: <code>bacon.sh restore SNAPSHOT_ID</code>
Might be important to clean up the data directory before restore.


## Docker Compose
<pre>
services:
    ...
    bacon:
        image: registry.gitlab.com/walnuss0815/bacon/mayan-edms:stable
        restart: always
        environment:
            RESTIC_REPOSITORY: "/backup"
            RESTIC_PASSWORD: "passw0rd"
            DATA_DIR: "/data"
            CRON: "0 22 * * 6"
            TZ: "Europe/Berlin"
            DB_HOST: "mayan"
            DB_NAME: "mayan"
            DB_USER: "mayan"
            DB_PASSWORD: "mayan-password"

        volumes:
            - ./backup:/backup
            - ./data:/data
    ...
</pre>