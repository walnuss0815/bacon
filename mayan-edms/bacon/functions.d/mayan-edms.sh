#!/bin/sh

select_command() {
    COMMAND=$1

    case "$COMMAND" in
        *)
            base_help
            ;;
    esac
}

help() {
    return
}

backup() {
    ## Copy files
    rsync -avzh "/data" "$DATA_DIR"

    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Database backup
    /usr/bin/pg_dump -h $DB_HOST -U $DB_USER -c $DB_NAME -w -C > "$DATA_DIR/backup.sql"
}

restore() {
    echo "Not implemented :("
}