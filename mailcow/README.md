# Mailcow

## Usage
Restore files to data directory: <code>bacon.sh restore SNAPSHOT_ID</code>
Might be important to clean up the data directory before restore.

### Restore database
To fully restore the database ensure its empty!
<pre>
mysql -h mysql-mailcow -u${DB_USER} -p${DB_PASSWORD} ${DB_NAME} < "$DATA_DIR/backup.sql"
</pre>

## Docker Compose
<pre>
services:
    ...
    bacon:
        depends_on:
            - mysql-mailcow
        image: registry.gitlab.com/walnuss0815/bacon/mailcow:latest
        restart: always
        environment:
            - RESTIC_REPOSITORY=/backup
            - RESTIC_PASSWORD=passw0rd
            - DATA_DIR=/data
            - CRON=0 22 * * 6
            - TZ=${TZ}
            - DB_USER=${DBUSER}
            - DB_PASSWORD=${DBPASS}
            - DB_NAME=${DBNAME}
        volumes:
            - vmail-vol-1:/var/vmail
        networks:
            mailcow-network:
                aliases:
                    - bacon
    ...
</pre>