#!/bin/sh

select_command() {
    COMMAND=$1

    case "$COMMAND" in
        *)
            base_help
            ;;
    esac
}

help() {
    return
}

backup() {
    ## Create directory
    mkdir -p "$DATA_DIR/vmail"

    ## Copy files
    rsync -avzh "/var/vmail" "$DATA_DIR"

    ## Database backup
    mysqldump --default-character-set=utf8mb4 -h mysql-mailcow -u${DB_USER} -p${DB_PASSWORD} ${DB_NAME} > "$DATA_DIR/backup.sql"
}

restore() {
    echo "Not implemented :("
}