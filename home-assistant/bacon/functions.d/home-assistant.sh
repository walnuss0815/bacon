#!/bin/sh

select_command() {
    COMMAND=$1

    case "$COMMAND" in
        *)
            base_help
            ;;
    esac
}

help() {
    return
}

backup() {
    ## Copy files
    rsync -avzh "/config" "$DATA_DIR"

# Disable backup of recorder data
#    ## Set database password
#    export PGPASSWORD=$DB_PASSWORD#
#
#    ## Database backup
#    /usr/bin/pg_dump -h home-assistant-db -U $DB_USER -c homeassistant -w -C > "$DATA_DIR/backup.sql"
}

restore() {
    echo "Not implemented :("
}