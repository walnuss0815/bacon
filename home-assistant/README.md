# Home Assistant

## Usage
Restore files to data directory: <code>bacon.sh restore SNAPSHOT_ID</code>
Might be important to clean up the data directory before restore.


## Docker Compose
<pre>
services:
    ...
    bacon:
        depends_on:
            - database
        image: registry.gitlab.com/walnuss0815/bacon/home-assistant:stable
        restart: always
        environment:
            RESTIC_REPOSITORY: "/backup"
            RESTIC_PASSWORD: "passw0rd"
            DATA_DIR: "/data"
            CRON: "0 22 * * 6"
            TZ: "Europe/Berlin"
            DB_USER: "homeassistant"
            DB_PASSWORD: "homeassistant-password"
        volumes:
            - ./backup:/backup
            - ./config:/config
    ...
</pre>