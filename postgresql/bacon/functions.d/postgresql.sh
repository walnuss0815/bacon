#!/bin/sh

select_command() {
    COMMAND=$1

    case "$COMMAND" in
        psql)
            psql
            break
		;;
        *)
            base_help
            ;;
    esac
}

help() {
    echo "  pgsql - PostgreSQL console"
}

backup() {
    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Database backup
    pg_dump -h $DB_HOST -U $DB_USER -c $DB_NAME -w -C > "$DATA_DIR/backup.sql"
}

restore() {
    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Database restore
    psql --set ON_ERROR_STOP=on -h $DB_HOST -U $DB_USER postgres -w < "$DATA_DIR/backup.sql"
}

psql() {
    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Connecto to database
    psql -h $DB_HOST -U $DB_USER $DB_NAME -w
}